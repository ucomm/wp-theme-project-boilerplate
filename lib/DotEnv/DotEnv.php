<?php

namespace ThemeBoilerplate\Dotenv;

/**
 * 
 * This class will read key/value pairs from a .env file and add them to the $_ENV global
 * 
 */
class Dotenv
{
  private $pairs;
  private $envVars;
  private $envPath;

  /**
   *
   * @param string $filePath - the absolute path to the .env file
   */
  public function __construct()
  {

    $this->envPath = $_SERVER['HTTP_HOST'] !== 'localhost' ?
      THEME_BOILERPLATE_DIR . '.env' :
      THEME_BOILERPLATE_DIR . '.env.example';

    $this->pairs = file($this->envPath);
    $this->setPairs();
  }

  /**
   * Add each enironment variable key/value pair to the $_ENV global
   *
   * @return void
   */
  public function createEnvGlobals()
  {
    foreach ($this->envVars as $key => $value) {
      $_ENV[$key] = $value;
    }
  }

  /**
   * Parses the .env file and puts the results into the $envVars array
   * NB - Only use letters or numbers for the credentials
   *
   * @return void
   */
  private function setPairs()
  {
    foreach ($this->pairs as $pair) {
      preg_match_all('/[\w\d-]+/', $pair, $matches);
      $this->envVars[$matches[0][0]] = $matches[0][1];
    }
  }
}
