<?php

namespace ThemeBoilerplate\Assets;

use ThemeBoilerplate\Assets\Loader;

/**
 * A class to handle loading CSS assets
 */
class StyleLoader extends Loader
{
  /**
   * Enqueue the prepared scripts.
   * In this example, the script will only be enqueued if the shortcode is present.
   *
   * @return void
   */
  public function enqueue()
  {
    $this->prepareAppStyles();

    wp_enqueue_script($this->handle);
  }

  /**
   * Prepare the styles by registering them. The directory for the script is determined by the environment.
   *
   * @return void
   */
  private function prepareAppStyles()
  {
    $styleDeps = [];
    wp_register_style(
      $this->handle,
      THEME_BOILERPLATE_URL . $this->buildDir . '/main.css',
      $styleDeps,
    );
  }



  /**
   * This method can be used to enqueue an asset on an admin page.
   * Use the slug to filter which pages it should be used on.
   *
   * @param string $hook - the admin page's slug to enqueue on
   * @return void
   */
  public function adminEnqueue(string $hook)
  {
    $settingsPage = stripos($hook, 'boilerplate');
    if ($settingsPage !== false) {
      $this->prepareAdminStyles();
      wp_enqueue_style($this->adminHandle);
    }
  }

  private function prepareAdminStyles()
  {
    $styleDeps = [];
    return wp_register_style(
      $this->adminHandle,
      THEME_BOILERPLATE_URL . 'admin-styles.css',
      $styleDeps
    );
  }
}
