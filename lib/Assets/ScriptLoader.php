<?php

namespace ThemeBoilerplate\Assets;

use ThemeBoilerplate\Assets\Loader;

/**
 * A class to handle loading JS assets
 */
class ScriptLoader extends Loader
{
    /**
     * Enqueue the prepared scripts.
     * In this example, the script will only be enqueued if the shortcode is present.
     *
     * @return void
     */
    public function enqueue()
    {
        $this->prepareAppScript();

        wp_enqueue_script($this->handle);
    }

    /**
     * This method can be used to enqueue an asset on an admin page.
     * Use the slug to filter which pages it should be used on.
     *
     * @param string $hook - the admin page's slug to enqueue on
     * @return void
     */
    public function adminEnqueue(string $hook)
    {
    }

    /**
     * Prepare the script by registering it.
     *
     * @return void
     */
    private function prepareAppScript()
    {
        $scriptDeps = [];
        wp_register_script(
            $this->handle,
            THEME_BOILERPLATE_URL . $this->buildDir . '/main.js',
            $scriptDeps,
            false,
            true
        );
    }
}
