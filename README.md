# WordPress Theme Boilerplate

Please see our Work in Progress documentation via Basecamp on how to use these boilerplates: [Basecamp Docs](https://public.3.basecamp.com/p/c1jrAMzZsQtbDDzRjdYg6Ptd)

This repository serves as a boilerplate WordPress theme, that works well with composer, docker, and local build tools.

The theme files are strikingly simple, and are intended as a starting point for any theme.  It is not based on any parent theme such as Cornerstone or Beaver Builder theme.  In this repository, you will not find examples of things such as customizer options, menu additions, etc.  It is a very barebones starting point.

## Get Started

So you want to create a theme using the boilerplate.  In order to do so, you will need a few things installed:

- [Composer](https://getcomposer.org/).  This is the cornerstone to our package management.
- [NodeJS](https://nodejs.org/en/).  Required for build steps in packages that you install. The best way to handle this is with [nvm](https://github.com/nvm-sh/nvm). 
- [Docker](https://www.docker.com/).  Community edition is fine.  Virtualizes a local server and can run local build chains.

Apologies in advance for needing all of these on your machine.  The goal down the line is to have all of these requirements eliminated except for Docker.  Pull requests are welcome (:

You are not required to clone this project in any way to start theme development.  Instead, the following should be run at the inception of a new theme:

```bash
composer create-project ucomm/wp-theme-project-boilerplate <new-theme-name>
cd <new-theme-name>
composer install
nvm use
npm install
```

The `composer create-project` command will reference [this repo via packagist](https://packagist.org/packages/ucomm/wp-plugin-project-boilerplate).

Next, you should run a search replace on the boilerplate directory to incorporate your theme name instead of boilerplate.

This will be an appropriate find/replace for the language namespace:

Find: `theme-boilerplate`

Replace: `<your-theme-slug>`

This will be an appropriate find/replace for the PHP namespace:

Find: `ThemeBoilerplate`

Replace: `<Your-Theme-Namespace>`

**Next, remove the `*.lock` entry from .gitignore.  This was in place purely for development on this boilerplate, as a normal theme should have dependencies locked in.**

Lastly, edit style.css to have the appropriate [WordPress theme metadata](https://developer.wordpress.org/themes/basics/main-stylesheet-style-css/#example).

To start development/see preview:

```bash
docker-compose up
nvm use
npm run dev
open http://localhost
```

### Local asset development
This boilerplate uses webpack to handle JS/CSS asset development. Webpack is (admittedly) complicated sometimes, but it: handles a wide variety of uses cases well, is under active development, has good documentation and examples. Will we use it forever? Probably not. To get started open another terminal window.

```bash
nvm use
npm run dev
```

### Viewing a project.
This project will be available at [localhost](http://localhost).

CSS and JS assets are shared between the local-dev container and web server. Please see the docker-compose file for how they are mounted/shared.

## Debugging and Testing

### Debugging
This repo allows debugging with the [VSCode PHP Debug extension](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug). A sample `launch.json` file is included in the repo for reference. Create a new `launch.json` file, copy/paste the contents and edit them to match the name of the theme. You will then be able to run xdebug! 

Need more detail? The xdebug functionality is provided via [our custom docker image](https://bitbucket.org/ucomm/docker-images/src/master/Dockerfile-wpcore-xdebug) based on the official WordPress docker image.

### Testing
PHP tests should be created using the [Codeception](https://codeception.com/) and [wp-browser](https://wpbrowser.wptestkit.dev/) testing frameworks. Please include tests! They take development time up front. But can save a lot of time (and embarrassment) later.

To see all possible codeception commands, run `./vendor/bin/codecept list`. To run a particular test, use
`./vendor/bin/codecept run [suite-name] [test-name]` for example `./vendor/bin/codecept run acceptance MyAcceptanceTest`

### Debugging Docker

You can access Docker containers directly from the shell (as long as they are running), example:

```bash
docker-compose exec web bash
```

## Adding Plugins/Dependencies

This project uses composer as a dependency management tool for PHP.  It also uses NPM/Yarn as a dependency management tool for local build tools, to compile/minify CSS/JS.

Packages can be found at:

- [Composer Packagist](https://packagist.org/)
- [WordPress Packagist](https://wpackagist.org/)
- [NPM Packages (JavaScript)](https://www.npmjs.com/)

To add one of our plugins, themes or other PHP dependencies, use the following format in the `composer.json` file

```json
{
  "repositories": [
      {
          "type": "composer",
          "url": "https://wpackagist.org"
      },
      {
        "type": "vcs",
        "url": "git@bitbucket.org/ucomm/{dependency-name}"
      }
  ]
}
```

Then run

```bash
composer require (--dev) ucomm/{dependency-name}
```

Yes, this makes the repository list really long, but makes updates really fast.

## Git
We use git. Please check that the default branch name for `git init` is set to `main`. You can confirm this by looking at your `gitconfig` file at `~/.gitconfig`. It should look like this

```bash
[init]
	defaultBranch = main
```

If it doesn't, please either add it there or use the following command
```
git config --global init.defaultBranch main
```

## Branching Strategy
### git flow
We try to use [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) as our branching strategy.  Not required, but helpful when working on a team.

If you are using git flow, please set the default git flow branch name to main as well. Your `gitconfig` file, should look like this

```bash
[gitflow "branch"]
	master = main
```

Or run 

```
git config --global gitflow.branch.master main
```
### Tagging
Tags must follow the [semver system](http://semver.org/). Tags can be added in git flow using the `git flow release start` and `git flow release finish` commands.

## Jenkins

### Building

In the Jenkins shell script, to create assets managed by webpack use the following format

```bash
# select the correct node version
source ~/.nvm/nvm.sh
nvm use

# install dependencies quickly
npm ci

# build assets
npm run build

# remove node_modules folders
find . -name "node_modules" -type d -exec rm -rf '{}' +
```

### Automating builds
In order to ensure automatic pushes to our development server(s) take the following steps
- Create a new [Jenkins project](http://ci.pr.uconn.edu:8080/) (either from scratch or by copying - see the [Castor project](http://ci.pr.uconn.edu:8080/job/Castor%20-%20Push%20to%20Dev%20(Aurora%20Sandbox)/) for an example)
- In bitbucket settings, go to `Settings -> Workflow -> Webhooks` and add a hook to Jenkins at the url `http://ci.pr.uconn.edu:8080/bitbucket-hook/`.
- In bitbucket settings, go to `Settings -> General -> Access Keys` and add the Jenkins ssh key. The key can be copy/pasted from another repo
- **If this project is going to be deployed to either the Aurora sandbox or health dev servers, make a new directory with the same name as the project using an ftp client. Otherwise the deployment may fail.**