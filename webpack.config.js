const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')

const prodEnv = process.env.NODE_ENV === 'production' ? true : false

const mode = !prodEnv ? 'development' : 'production'
const buildDir = !prodEnv ? 'dev-build' : 'dist'

const optimization = {}

if (prodEnv) {
  optimization.minimize = true
  optimization.minimizer = [
    new CssMinimizerPlugin(),
    '...'
  ]
}

module.exports = {
  entry: [
    // using core js enables polyfills and helps if you want to make ajax requests
    // 'core-js/stable',
    // 'regenerator-runtime/runtime',
    path.resolve(__dirname, 'src/js/index.js')
  ],
  output: {
    path: buildDir,
    filename: '[name].js',
    chunkFilename: '[name].js'
  },
  mode,
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    debug: true,
                    useBuiltIns: 'entry',
                    corejs: 3.22
                  }
                ],
                '@babel/preset-react'
              ]
            }
          }
        ]
      },
      /**
       * 
       * Use this if you intend to bundle style assets as well.
       * Make sure to install the appropriate loaders etc...
       * 
       */
      {
        test: /\.s?css$/i,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              importLoaders: 2,
            }
          },
          {
            loader: 'postcss-loader'
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              sassOptions: {
                includePaths: [
                  'node_modules/scss/**/*.scss'
                ]
              }
            }
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.css', '.scss']
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css'
    })
  ],
}