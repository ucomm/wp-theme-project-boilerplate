<?php

use ThemeBoilerplate\Assets\ScriptLoader;
use ThemeBoilerplate\Assets\StyleLoader;

// Constants
define( 'THEME_BOILERPLATE_DIR', get_stylesheet_directory() );
define( 'THEME_BOILERPLATE_URL', get_stylesheet_directory_uri() );

// Load
// select the right composer autoload.php file depending on environment.
if (file_exists(dirname(ABSPATH) . '/vendor/autoload.php')) {
	require_once(dirname(ABSPATH) . '/vendor/autoload.php');
} elseif (file_exists(ABSPATH . 'vendor/autoload.php')) {
	require_once(ABSPATH . 'vendor/autoload.php');
} else {
	require_once('vendor/autoload.php');
}

// require files from lib
require_once( 'lib/Admin/Options.php' );
require_once('lib/Admin/SettingsPage.php');
require_once('lib/Assets/Loader.php');
require_once('lib/Assets/ScriptLoader.php');
require_once('lib/Assets/StyleLoader.php');
require_once('lib/DotEnv/DotEnv.php');
require_once('lib/Endpoints/Controller.php');
require_once('lib/Endpoints/Example.php');

// Enqueue Scripts and Styles
$scriptLoader = new ScriptLoader();
$styleLoader = new StyleLoader();

if (!is_admin()) {
	$sciptLoader->enqueueAssets();
	$styleLoader->enqueueAssets();
} else {
	$sciptLoader->enqueueAdminAssets();
	$styleLoader->enqueueAdminAssets();
}